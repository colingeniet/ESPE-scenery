#!/bin/bash

MIN_LON=19
MAX_LON=23
MIN_LAT=65
MAX_LAT=68

for lon in $(seq ${MIN_LON} $((${MAX_LON} - 1))); do
    for lat in $(seq ${MIN_LAT} $((${MAX_LAT} - 1))); do
        lon10=$((${lon}/10*10))
        lat10=$((${lat}/10*10))
        printf "e%03dn%02d/e%03dn%02d " ${lon10} ${lat10} ${lon} ${lat}
    done
done
