#!/bin/bash

THREADS=5

rm -r build/DEM
mkdir -p build/DEM

gdalchop build/DEM ${PWD}/data/eu_dem.tiff

terrafit -x 10000 -e 5 build/DEM -j ${THREADS}
