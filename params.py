# OSM2CITY parameters

### These parameters need to be adjusted.

PATH_TO_SCENERY = "/home/colin/flightgear/Scenery-dev/ESPE"

PATH_TO_OSM2CITY_DATA = "/home/colin/flightgear/Scenery-dev/osm2city-data"
DB_NAME = "osm2city"
DB_USER = "colin"
DB_USER_PASSWORD = ""

FG_ELEV = "fgelev"


### Other parameters can be left as is.

PREFIX = "ESPE"
PATH_TO_OUTPUT = None   # default: same as scenery

FLAG_STG_BUILDING_LIST = True
BUILDING_REDUCE_THRESHOLD = 0.0
CLUSTER_MIN_OBJECTS = 1.0

OVERLAP_CHECK_APT_PAVEMENT_BUILDINGS_INCLUDE = ["ESPE", "ESNQ", "ESNJ"]
