import argparse
import sys

import osm2city.utils.coordinates as co


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Write a TREE_LIST file from a list of 'lon lat elev' coordinates")
    parser.add_argument("center_lon", type=float)
    parser.add_argument("center_lat", type=float)
    parser.add_argument("center_elev", type=float, nargs="?", default=0.0)
    args = parser.parse_args()

    center = co.Vec2d(args.center_lon, args.center_lat)
    transform = co.Transformation(center)

    for line in sys.stdin:
        parts = line.split()
        if not parts:
            continue
        if len(parts) != 3:
            sys.stderr.write("Unexpected input line: " + line)

        x, y = transform.to_local((float(parts[0]), float(parts[1])))
        elev = float(parts[2]) - args.center_elev

        print("{:7.1f} {:7.1f} {:7.1f}".format(-y, x, elev))
