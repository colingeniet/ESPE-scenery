#!/bin/bash

if [[ $# -eq 0 ]]; then
    airports=$(./airports_list.sh)
else
    airports=$@
fi

rm -r build/Airport{Area,Obj}

ESNQ_done=

for apt in ${airports}; do
    if [[ ${apt} == ESNQ ]] || [[ ${apt} == ESUK ]]; then
        [[ -z ${ESNQ_done} ]] || continue
        ESNQ_done=1
        # Airports are closed, need to be processed together. Concatenate into temporary file.
        apt="ESNQ_ESUK"
        sed '/^99$/ d' NavData/apt/ESNQ.dat > NavData/apt/${apt}.dat
        sed '/^I$/ d; /^1130$/ d' NavData/apt/ESUK.dat >> NavData/apt/${apt}.dat
    fi
    genapts850 --input=NavData/apt/${apt}.dat --work=build --dem-path=DEM --max-slope=0.1

    if [[ ${apt} == ESNQ_ESUK ]]; then
        rm NavData/apt/${apt}.dat
    fi
done
