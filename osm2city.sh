#!/bin/bash

THREADS=4
OSM2CITY_DIR="${HOME}/flightgear/Scenery-dev/osm2city"

BUILD_TILES="${OSM2CITY_DIR}/build_tiles.py"

mkdir osm2city-build
cd osm2city-build

function osm () {
    env PYTHONPATH="${OSM2CITY_DIR}:${PYTHONPATH}" python3 "${BUILD_TILES}" -f ../params.py -b $1 -p ${THREADS} -e all
}

if [[ $# -eq 0 ]]; then
    # default, all terrain
    osm 19_65_23_68
else
    # Arguments should be a list of airports
    for icao in $@; do
        case ${icao} in
            ESPE) osm "20.0_65.8_20.3_65.9";;
            ESPA) osm "22.0_65.5_22.2_65.6";;
            ESNX) osm "19.1_65.5_19.4_65.7";;
            ESNP) osm "21.2_65.3_21.3_65.5";;
            ESUV) osm "21.0_65.6_21.1_65.7";;
            ESNJ) osm "20.0_66.4_20.2_66.6";;
            ESNG) osm "20.7_67.1_20.9_67.2";;
            ESNQ|ESUK) osm "20.2_67.7_20.4_67.9";;
            *) echo -en "usage: $0 [ICAO ...]\nICAO must be among the ones in ./airports_list.sh\n"; exit 1;;
        esac
    done
fi
