#!/bin/bash

APT_DIR=NavData/apt/
APT="${APT_DIR}/ESPE.dat"
APT_BCK="${APT_DIR}/ESPE.dat.bck"

BTG="build/AirportObj/e020n60/e020n65/ESPE.btg"


# Changes to the file coming from WED
# Rename runway 12R/30L to 12D/30D
echo "Apply edit to raw WED ESPE.dat file (no-op if already done)."
sed -i '/^100 / s/12R/12D/;s/30L/30D/' "${APT}"

# Backup the file, we will do some temporary edits for terrain generation.
echo "Backing up ESPE.dat"
cp "${APT}" "${APT_BCK}"

# Rename runway 12C/30C to 7/7 -> allows simple edits in the btg file.
# Rename runway 12D/30D to XX/XX -> disables runway numbers in terrain generation.
echo "Editing ESPE.dat for genapts hacks."
sed -i '/^100 / s/12C/7/;s/30C/7/;s/12D/XX/;s/30D/XX/' "${APT}"

# Run genapts850
echo "./genapts.sh $@"
./genapts.sh $@

# Restore the APT.dat file.
echo "Restoring ESPE.dat"
mv "${APT_BCK}" "${APT}"

# Now we need to edit the btg file. First extract it.
echo "Extracting ESPE.btg"
gunzip "${BTG}.gz"

# Edits
echo "Editing ESPE.btg"
# Runway C was renamed to 7/7 -> now there is material pa_7c. Relpace it with pa_C.
# Also edit the length of the field (4 bytes just before the field itself).
bbe -e 's/\x05\x00\x00\x00pa_7c/\x04\x00\x00\x00pa_C/' "${BTG}" > "${BTG}.new"
mv "${BTG}.new" "${BTG}"

# Finally compress it back
echo "Compressing ESPE.btg"
gzip "${BTG}"

echo "Ready to run terragear!"
