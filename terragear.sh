#!/bin/bash

THREADS=5

GEN_DIRS="Default DEM AirportArea AirportObj"
CLC_DIRS="Urban SubUrban Industrial Transport Port Airport OpenMining Dump Construction Greenspace GolfCourse DryCrop IrrCrop Rice Vineyard Orchard Olives MixedCrop MixedCrop ComplexCrop NaturalCrop AgroForest DeciduousForest EvergreenForest MixedForest Grassland Heath Sclerophyllous Scrub Sand Rock HerbTundra Burnt Glacier Marsh Bog SaltMarsh Saline Littoral Watercourse Lake Lagoon Estuary Ocean"
# already loaded from CLC: Watercourse
OSM_DIRS="Road Railroad Canal Stream"

# Arguments: min-lon, max-lon, min-lat, max-lat
function tg () {
    tg-construct --threads=${THREADS} --work-dir=build --output-dir=Terrain --min-lon=$1 --max-lon=$2 --min-lat=$3 --max-lat=$4 \
        ${GEN_DIRS} ${CLC_DIRS} ${OSM_DIRS} # Don't add quotes here!
}

if [[ $# -eq 0 ]]; then
    # default, all terrain
    # should be tg 19 23 65 68, but SG is bugged and causes TG to build one more bucket at the north boundary.
    tg 19 23 65 67.875
else
    # Arguments should be a list of airports
    for icao in $@; do
        case ${icao} in
            ESPE) tg 20.0 20.3 65.8 65.9;;
            ESPA) tg 22.0 22.2 65.5 65.6;;
            ESNX) tg 19.1 19.4 65.5 65.7;;
            ESNP) tg 21.2 21.3 65.3 65.5;;
            ESUV) tg 21.0 21.1 65.6 65.7;;
            ESNJ) tg 20.0 20.2 66.4 66.6;;
            ESNG) tg 20.7 20.9 67.1 67.2;;
            ESNQ|ESUK) tg 20.2 20.4 67.7 67.9;;
            *) echo -en "usage: $0 [ICAO ...]\nICAO must be among the ones in ./airports_list.sh\n"; exit 1;;
        esac
    done
fi
