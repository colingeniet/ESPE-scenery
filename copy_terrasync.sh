#!/bin/bash

[[ -z "${FG_HOME}" ]] && FG_HOME="${HOME}/.fgfs/"

TERRASYNC="${FG_HOME}/TerraSync"

for coord in $(./scenery_folders.sh); do
    for dir in Objects Pylons; do
        path="${dir}/${coord}"
        tsc_path="${TERRASYNC}/${path}"
        [[ -d ${tsc_path} ]] || continue

        rm -r "${path}"
        mkdir -p "${path}"
        cp -r "${tsc_path}"/* "${path}"
    done
done
