#!/bin/bash

[[ -z "${FG_HOME}" ]] && FG_HOME="${HOME}/.fgfs/"

TERRASYNC="${FG_HOME}/TerraSync"
# Set appropriately, or define as environment variable:
# FG_ROOT=

res=0

function ac_dependencies {
    local dir="${1%/*}"
    local file
    while read file; do
        [[ -z ${file} ]] && continue
        copy_file "${dir}/${file}"
    done <<<$(cat "$1" | tr -d '\r' | awk '/^texture/ { print $2 }' | sort | uniq | tr -d '"')
}

function xml_dependencies {
    local dir="${1%/*}"
    local file
    while read file; do
        [[ -z ${file} ]] && continue
        if [[ !(${file} =~ ^/?Models/.*) ]]; then file="${dir}/${file}"; fi
        copy_file "${file}"
    done <<<$(cat "$1" | tr -d '\r' | sed -n '/<path>\(.*\)<\/path>/ {s/.*<path>//; s/<\/path>.*//; p}' | sort | uniq)
}

function copy_dependencies {
    local ext="${1##*\.}"
    case ${ext} in
        ac) ac_dependencies "$1";;
        xml) xml_dependencies "$1";;
    esac
}

function copy_file {
    local file="$1"
    # For files in FGData, nothing to do
    if [[ -f ${FG_ROOT}/${file} ]]; then return; fi
    # Try to copy from Terrasync
    if [[ !( -f ${file} ) ]]; then
        # If the file is missing, copy it
        if [[ -f ${TERRASYNC}/${file} ]]; then
            echo "Copying from Terrasync: ${file}"
            mkdir -p "./${1%/*}"
            cp -n "${TERRASYNC}/${file}" "${file}"
        fi
    fi
    # Check dependencies (even if the file was present, in case of partial copy)
    if [[ -f ${file} ]]; then
        copy_dependencies "${file}"
    else
        echo "Missing file ${file}"
        res=1
    fi
}

# Find all OBJECT_SHARED
while read file; do
    copy_file "${file}"
done <<<$(awk '/^OBJECT_SHARED/ { print $2 }' $(find ./ -iname '*.stg') | sort | uniq)

exit ${res}
